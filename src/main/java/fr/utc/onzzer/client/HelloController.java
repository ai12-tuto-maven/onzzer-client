package fr.utc.onzzer.client;

import fr.utc.onzzer.common.User;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.util.UUID;

public class HelloController {

    private final User user = new User(UUID.randomUUID(), "Jean Mickael", "JM60");

    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        String text = String.format("Hello %s. Your UUID is : %s.", this.user.getUsername(), this.user.getUUID());
        this.welcomeText.setText(text);
    }
}