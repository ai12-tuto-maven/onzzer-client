module fr.utc.onzzer.onzzerclient {
    requires javafx.controls;
    requires javafx.fxml;
    requires onzzer.common;


    opens fr.utc.onzzer.client to javafx.fxml;
    exports fr.utc.onzzer.client;
}